import { getPacketType } from '../PacketReader';
import { PacketType } from '../PacketType';
import { PlayerPackets } from '../Responses/PlayerPackets';
import type { PlayerConnection } from '../Server/Game/GameConnection';
import type { PacketConnection } from '../Server/Packet/PacketConnection';
import { AbstractPacketHandler } from './AbstractPacketHandler';

/**
 * Handles packets regarding basic player data.
 */
export class PlayerDataPacketHandler extends AbstractPacketHandler {
	/**
	 * Checks if this packet handler is used to handle a packet type.
	 * @param type
	 */
	public handlesType(type: PacketType): boolean {
		return (
			type === PacketType.PlayerInformation ||
			type === PacketType.PlayerUseStats ||
			type === PacketType.PlayerResist ||
			type === PacketType.PlayerSkills
		);
	}

	/**
	 * Handles the given packet.
	 * @param packet
	 * @param client
	 */
	public handlePacket(packet: Buffer, client: PacketConnection): void {
		if (!this.hasPlayer(client)) return;

		let type = getPacketType(packet);

		switch (type) {
			// Get all stats
			case PacketType.PlayerInformation:
				client.write(PlayerPackets.information(client.player));
				break;

			// Use stat points
			case PacketType.PlayerUseStats:
				this.onStats(packet, client);
				break;

			// Get resist list
			case PacketType.PlayerResist:
				client.write(PlayerPackets.resist(client.player));
				break;

			// Retrieve skill list
			case PacketType.PlayerSkills:
				client.write(PlayerPackets.skills(client.player));
				break;

			default:
				this.notImplemented(packet);
		}
	}

	/**
	 * Happens when the player assigns stat points.
	 * @param packet
	 * @param client
	 */
	private onStats(packet: Buffer, client: PlayerConnection): void {
		let sta = packet.readUInt16LE(18);
		let int = packet.readUInt16LE(20);
		let str = packet.readUInt16LE(22);
		let agi = packet.readUInt16LE(24);

		let sum = sta + int + str + agi;

		if (client.player.fightData.stats.unused < sum) return;

		client.player.fightData.stats.unused -= sum;
		let stats = client.player.fightData.stats;
		stats.hp.pointsBase += sta;
		stats.mp.pointsBase += int;
		stats.attack.pointsBase += str;
		stats.speed.pointsBase += agi;

		// TODO update things?

		client.write(PlayerPackets.useStats(client.player));
	}
}
