import type { NpcJson } from '../../../Database/Collections/Npc/NpcJson';
import { Direction } from '../../../Enums/Direction';

export const DesCityNpcsData: NpcJson[] = [
	{
		id: 0x80000159,
		name: 'Narrow Mine',
		file: 144,
		map: 12,
		point: { x: 1456, y: 5304 },
		direction: Direction.SouthWest,
		action: {
			type: 'teleport',
			targetNpcId: 0x80000123,
		},
	},
	{
		id: 0x80000161,
		name: 'Wanderer',
		file: 159,
		map: 12,
		point: { x: 1680, y: 1840 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000162,
		name: 'Enchanter',
		file: 129,
		map: 12,
		point: { x: 1936, y: 2632 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000163,
		name: 'Bush the Miner',
		file: 159,
		map: 12,
		point: { x: 2080, y: 4960 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000164,
		name: 'Blacksmith',
		file: 135,
		map: 12,
		point: { x: 2208, y: 2536 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000165,
		name: 'Bank Teller',
		file: 113,
		map: 12,
		point: { x: 2704, y: 3360 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000166,
		name: 'Citizen Hans',
		file: 159,
		map: 12,
		point: { x: 2720, y: 2000 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000167,
		name: 'Groceror',
		file: 101,
		map: 12,
		point: { x: 3200, y: 3328 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000168,
		name: 'Spirit Mage',
		file: 159,
		map: 12,
		point: { x: 3200, y: 480 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000169,
		name: 'Armor Seller',
		file: 159,
		map: 12,
		point: { x: 4288, y: 3648 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000170,
		name: 'Town Guide',
		file: 111,
		map: 12,
		point: { x: 4768, y: 3520 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000172,
		name: 'Blazing Titan',
		file: 244,
		map: 12,
		point: { x: 4848, y: 2952 },
		direction: Direction.SouthEast,
	},
	{
		id: 0x80000173,
		name: 'Moss Titan',
		file: 232,
		map: 12,
		point: { x: 4960, y: 2904 },
		direction: Direction.SouthEast,
	},
	{
		id: 0x80000174,
		name: 'Gusting Titan',
		file: 230,
		map: 12,
		point: { x: 5056, y: 2848 },
		direction: Direction.SouthEast,
	},
	{
		id: 0x80000175,
		name: 'Buyer',
		file: 129,
		map: 12,
		point: { x: 5072, y: 3360 },
		direction: Direction.SouthEast,
	},
	{
		id: 0x80000176,
		name: 'Girl Miya',
		file: 104,
		map: 12,
		point: { x: 5120, y: 3640 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000177,
		name: 'Scott',
		file: 159,
		map: 12,
		point: { x: 5200, y: 1504 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000178,
		name: 'Shopkeeper',
		file: 159,
		map: 12,
		point: { x: 5232, y: 1840 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000179,
		name: 'Healer (Shop)',
		file: 103,
		map: 12,
		point: { x: 5728, y: 3096 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000180,
		name: 'Worn Messenger',
		file: 159,
		map: 12,
		point: { x: 6192, y: 3032 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000181,
		name: 'Kang Elder',
		file: 159,
		map: 12,
		point: { x: 6368, y: 1784 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000182,
		name: 'Help Newbie',
		file: 159,
		map: 12,
		point: { x: 6512, y: 3800 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000184,
		name: 'Sleep Dragon',
		file: 245,
		map: 12,
		point: { x: 7424, y: 376 },
		direction: Direction.SouthEast,
	},
];
