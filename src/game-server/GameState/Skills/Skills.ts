import type { SkillsGroupJson } from './SkillsGroup';
import { SkillsGroup } from './SkillsGroup';

export class Skills {
	public skillData: SkillsGroup[] = [];

	private constructor() {}

	/**
	 * Adds a skill to the member if it does not exist
	 * @param id
	 * @param exp
	 */
	public addSkill(id: number, exp: number): void {
		let skill = SkillsGroup.fromExp(id, exp);
		this.skillData.push(skill);
	}

	/**
	 * Adds multiple skills to the member if it does not exist
	 * @param skills
	 */
	public addSkills(skills: SkillsGroupJson[]): void {
		for (let skill of skills.values()) {
			this.addSkill(skill.id, skill.exp);
		}
	}

	/**
	 * Turn skills into storable object.
	 * @returns
	 */
	public toJson(): SkillsGroupJson[] {
		let json: SkillsGroupJson[] = [];
		for (let skill of this.skillData) {
			json.push(skill.toJson());
		}
		return json;
	}

	/**
	 * Recreate skills from stored information
	 * @param skills
	 * @returns
	 */
	public static fromJson(skills: SkillsGroupJson[]): Skills {
		let skillData = new Skills();
		skillData.addSkills(skills);
		return skillData;
	}
}
