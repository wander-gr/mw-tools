import { skillProperties } from '../../Data/SkillProperties';
import { FightActionType } from '../../Enums/FightActionType';
import { FightEffect } from '../../Enums/FightEffect';
import { SkillType } from '../../Enums/SkillType';
import { Logger } from '../../Logger/Logger';
import type { SkillsGroup } from '../Skills/SkillsGroup';
import { EffectApply } from './EffectApply';
import type { FightAction } from './FightAction';
import type { FightActionResult } from './FightActionResultTypes';
import type { FightMember } from './FightMember';
import { SkillTargeting } from './SkillTargeting';

export class FightActionSkill {
	/**
	 * A fightmember uses a skill.
	 * @param action
	 * @param skillData
	 */
	public static execute(action: FightAction, skillData: SkillsGroup): FightActionResult {
		let skill = skillProperties[skillData.id];
		let targets = SkillTargeting.getBySpeed(action);

		let result: FightActionResult = {
			type: skill.single ? FightActionType.Magic : FightActionType.MultiMagic,
			source: action.member.base.id,
			target: action.target ? action.target.base.id : 0,
			detail: skill.id,
			stats: [action.member],
			magic: [],
			data: null,
		};

		switch (skill.type) {
			case SkillType.Fire:
			case SkillType.Ice:
			case SkillType.Evil:
			case SkillType.Flash:
				targets.forEach(target => this.executeMagicSkill(result, target, skillData));
				break;
			case SkillType.Frailty:
			case SkillType.Poison:
			case SkillType.Chaos:
			case SkillType.Hypnotize:
			case SkillType.Stun:
			case SkillType.PurgeChaos:
			case SkillType.UnStun:
			case SkillType.Speed:
			case SkillType.Death:
			case SkillType.Enhance:
			case SkillType.Protect:
			case SkillType.Reflect:
			case SkillType.Repel:
				targets.forEach(target => this.executeEffectSkill(result, target, skillData));
				break;
			case SkillType.MultiShot:
			case SkillType.Blizzard:
			case SkillType.HealOther:
			case SkillType.Drain:
				Logger.warn('Skill not implemented: ' + skill.type);
				break;
		}

		return result;
	}

	/**
	 * Apply fire, ice, evil or flash to the target and update the result.
	 * @param result
	 * @param target
	 * @param skillData
	 */
	private static executeMagicSkill(
		result: FightActionResult,
		target: FightMember,
		skillData: SkillsGroup,
	): void {
		let baseDamage = 50;
		// TODO resist
		let damage = skillData.effect ?? baseDamage;
		// TODO repel
		let repel = 0;

		target.base.fightData.stats.addHp(-damage);

		// TODO use copy of hp in FightMember, update dead-status in setter
		if (target.base.fightData.stats.currentHp === 0) target.effect.value = FightEffect.Dead;

		result.magic?.push({
			id: target.base.id,
			damage,
			repel,
		});

		result.stats.push(target);
	}

	/**
	 * Apply effect skills and update the result.
	 * @param result
	 * @param target
	 * @param skillData
	 */
	private static executeEffectSkill(
		result: FightActionResult,
		target: FightMember,
		skillData: SkillsGroup,
	): void {
		// TODO resist
		let chance = 0.95;

		if (Math.random() > chance) return;

		this.setEffect(target, skillData);
		result.stats.push(target);
	}

	/**
	 * Apply the effect of a skill.
	 * @param member
	 * @param skillData
	 */
	private static setEffect(member: FightMember, skillData: SkillsGroup): void {
		let skill = skillProperties[skillData.id];

		if (skill.type === SkillType.PurgeChaos) {
			member.effect.remove(FightEffect.Chaos);
			member.effectCounters.set(FightEffect.Chaos, 0);
			return;
		}

		if (skill.type === SkillType.UnStun) {
			member.effect.remove(FightEffect.Stun);
			member.effectCounters.set(FightEffect.Stun, 0);
			return;
		}

		// TODO rounds depend on level
		let rounds = skill.rounds;
		let effect = EffectApply.skillEffectMap[skill.type];

		if (!effect) return;

		let applied = EffectApply.apply(member.effect, effect);

		if (applied) {
			member.effectCounters.set(effect, rounds);
			member.removeUnusedCounters();
		}
	}
}
