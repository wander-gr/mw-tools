import type { IPlayerSettings } from '../../GameState/Player/IPlayerSettings';
import type { Packet } from '../../PacketBuilder';
import { ToggleSet } from '../../Utils/ToggleSet';
import type { ChatConnection } from './ChatConnection';

/**
 * Keeps track of who has what chat channel enabled,
 * and offers a way to send packets to those players.
 * TODO lots
 */
export class ChatServerChannelWriter {
	private localChatConnections: ToggleSet<ChatConnection> = new ToggleSet();
	private worldChatConnections: ToggleSet<ChatConnection> = new ToggleSet();

	/**
	 * Update the user settings for a connection.
	 * @param client
	 * @param settings
	 */
	public updateSettings(client: ChatConnection, settings: IPlayerSettings): void {
		this.localChatConnections.set(client, settings.localChatEnabled);
		this.worldChatConnections.set(client, settings.worldChatEnabled);
	}

	/**
	 * Send a packet to EVERYONE who has local chat enbled.
	 * TODO only to people near you on the same map
	 * @param packet
	 */
	public local(packet: Packet): void {
		for (let con of this.localChatConnections) con.write(packet);
	}

	/**
	 * Send a packet to everyone who has world chat enabled.
	 * @param packet
	 */
	public world(packet: Packet): void {
		for (let con of this.worldChatConnections) con.write(packet);
	}
}
