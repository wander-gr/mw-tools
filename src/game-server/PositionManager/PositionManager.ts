import { Worker } from 'worker_threads';
import type { Game } from '../GameState/Game';
import type { GameMap } from '../GameState/Map/GameMap';
import type { Player } from '../GameState/Player/Player';
import { ResourceManager } from '../ResourceManager/ResourceManager';
import { MapPackets } from '../Responses/MapPackets';
import type { Point } from '../Utils/Point';
import type { MapFileJson } from './Server/MapLinks';
import { MapLinks } from './Server/MapLinks';
import { ServerPositionPlayerData } from './Server/ServerPositionPlayerData';
import type { PositionWorkerParams } from './Worker/PositionWorker';
import type { PositionUpdate } from './Worker/WorkerPositionPlayerData';
import { Random } from '../Utils/Random';

/**
 * Manages the players' positions, map changes and so on.
 */
export class PositionManager {
	private readonly resMapData = ResourceManager.get('mapData') as MapFileJson[];
	private readonly resMapCells = ResourceManager.get('mapCells');
	private readonly workerPath: string = module.path + '/Worker/PositionWorker.js';
	private readonly worker: Worker;
	private readonly playerData: SharedArrayBuffer;
	private readonly positionPlayerData: ServerPositionPlayerData;
	private readonly mapLinks: Map<number, MapLinks> = new Map();
	private game!: Game;

	public constructor() {
		let size = ServerPositionPlayerData.getBufferSize();
		this.playerData = new SharedArrayBuffer(size);
		this.worker = this.createWorker();
		this.positionPlayerData = new ServerPositionPlayerData(this.playerData);
	}

	/**
	 * Called when the game object is initialised.
	 * @param game
	 */
	public initGame(game: Game): void {
		this.game = game;

		this.resMapData.forEach(data => {
			let cells = this.resMapCells.find(x => x.map === data.map);

			if (!cells) return;

			this.mapLinks.set(data.map, new MapLinks(data, game.maps, cells.buffer));
		});

		this.worker.on('message', (updates: PositionUpdate[]) => this.onWorkerUpdate(updates));
	}

	/**
	 * Called by the Game-class when a player enters the game.
	 * The player's mapData should be set before this.
	 * @param player
	 */
	public onPlayerEnter(player: Player): void {
		player.mapData.map.players.set(player.id, player);
		// TODO check if player is in fight

		player.client?.write(
			...MapPackets.mapData(player),
			MapPackets.npcList(player),
			MapPackets.enter,
		);

		player.mapData.map.sendPacket(MapPackets.npcAdd([player]), player);
		this.positionPlayerData.addPlayer(player);
	}

	/**
	 * Called when the player leaves the game.
	 * @param player
	 */
	public onPlayerLeave(player: Player): void {
		player.mapData.map.players.delete(player.id);
		player.mapData.map.sendPacket(MapPackets.npcRemove([player]), player);
		this.positionPlayerData.removePlayer(player.id);
	}

	/**
	 * Called when a player wants to walk somewhere.
	 * @param player
	 * @param x
	 * @param y
	 */
	public onDestChange(player: Player, dest: Point): void {
		player.mapData.dest = dest;
		player.mapData.map.sendPacket(MapPackets.walkDestinations([player]));
		this.positionPlayerData.writeDestToBuffer(player);
	}

	/**
	 * Called when the player moves to another map.
	 * @param player
	 * @param newMap
	 */
	public onRequestMapChange(player: Player, newMap: GameMap, point: Point): void {
		player.mapData.map.players.delete(player.id);
		player.mapData.map.sendPacket(MapPackets.npcRemove([player]), player);

		player.mapData.map = newMap;
		player.mapData.point = point;
		player.mapData.dest = point;
		newMap.players.set(player.id, player);
		this.positionPlayerData.updatePlayerMap(player);

		player.client?.write(
			...MapPackets.mapData(player),
			MapPackets.npcList(player),
			MapPackets.change,
		);

		newMap.sendPacket(MapPackets.npcAdd([player]), player);
	}

	/**
	 * Called when the worker sends an update on player positions.
	 * @param updates
	 */
	private onWorkerUpdate(updates: PositionUpdate[]): void {
		updates.forEach(update => {
			let player = this.game.players.get(update.id);

			if (!player) return;

			this.positionPlayerData.writeCoordinatesToPlayer(player);

			let mapLinks = this.mapLinks.get(player.mapData.map.file);
			let link = mapLinks?.getMapLink(player.mapData.point);

			if (link?.dest) {
				let dest = link.getDestinationCoordinates();
				this.onRequestMapChange(player, link.dest, dest);
			} else if (
				player.mapData.map.hasRandomFights &&
				player.mapData.map.fightFrequency > 0
			) {
				player.memory.stepCount += update.steps;
				/**
				 * Random number to add to stepCount
				 * Requires atleast 10% steps before you can enter combat
				 */
				let randomStep = Random.intInclusive(0, player.mapData.map.fightFrequency * 0.9);

				if (player.memory.stepCount + randomStep > player.mapData.map.fightFrequency) {
					player.memory.stepCount = 0;
					player.mapData.map.startRandomFight(player);
				}
			}
		});
	}

	/**
	 * Create the worker that updates player positions.
	 */
	private createWorker(): Worker {
		let workerData: PositionWorkerParams = {
			playerData: this.playerData,
			mapData: this.resMapData.map(data => ({
				map: data.map,
				width: data.width,
				height: data.height,
				cells: this.resMapCells.find(x => x.map === data.map)!.shared,
			})),
		};

		let worker = new Worker(this.workerPath, { workerData });

		return worker;
	}
}
