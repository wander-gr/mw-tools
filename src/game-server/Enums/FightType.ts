/** Used to specify what type of fight it is
 * Monster is random monster encounter
 * Player is PVP
 */
export const enum FightType {
	Monster = 0,
	Player = 1,
}
